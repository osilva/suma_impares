# Repositorio plantilla: "Suma de números impares"
Construir un programa que realice la suma de todos los números impares comprendidos entre dos números enteros no negativos introducidos 
por teclado por el usuario.
Al arrancar, el programa escribirá: Dame un número entero no negativo: . 
Cuando el usuario escriba el número, el programa escribirá: Dame otro: . 
A continuación, el programa mostrará por pantalla la suma de los números impares comprendidos 
entre esos dos números, incluidos cualquiera de ellos si es un número impar.
Llama al programa impares.py.
Para entregar este ejercicio, crea una bifurcación (fork) de este repositorio, y sube a él tu solución. Puedes consultar el enunciado, que incluye la fecha de entrega.

